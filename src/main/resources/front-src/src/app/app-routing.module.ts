import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegisterComponent} from "./register/register.component";
import {HomeComponent} from "./home/home.component";
import { DietchooserComponent } from './dietchooser/dietchooser.component';


const routes: Routes = [
    { path: 'register', component: RegisterComponent },
    { path: 'home', component: HomeComponent},
    { path: 'diet/:firstName/:lastName', component: DietchooserComponent},
    { path: '**', redirectTo: 'home'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
