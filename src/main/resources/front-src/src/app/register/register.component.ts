import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    form: FormGroup;

    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.form = this.createRegisterForm();
    }

  private createRegisterForm() {
    return this.formBuilder.group({
      firstName: this.formBuilder.control("", Validators.required),
      lastName: this.formBuilder.control("", Validators.required),
      email: this.formBuilder.control("", [Validators.required, Validators.email]),
      gender: this.formBuilder.control("", Validators.required),
      country: this.formBuilder.control("", Validators.required),
    });
  }

  submitForm() {
    console.log(this.form.value);
  }
}
