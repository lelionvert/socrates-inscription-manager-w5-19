import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dietchooser',
  templateUrl: './dietchooser.component.html',
  styleUrls: ['./dietchooser.component.scss']
})
export class DietchooserComponent implements OnInit {

  firstName = '';
  lastName = '';

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.firstName = this.route.snapshot.params.firstName;
    this.lastName = this.route.snapshot.params.lastName;
  }

}
