import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietchooserComponent } from './dietchooser.component';

describe('DietchooserComponent', () => {
  let component: DietchooserComponent;
  let fixture: ComponentFixture<DietchooserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietchooserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietchooserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
