package com.example.candidate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CandidateService {

    @Autowired
    CandidateRepository candidateRepository;

    public List<Candidate> getAllCandidate() {
        return candidateRepository.findAll();
    }

    public void save(Candidate candidate) {
        candidateRepository.save(candidate);
    }

    public boolean isCandidateExist(String email) {
        return candidateRepository.existsByEmail(email);
    }
}
