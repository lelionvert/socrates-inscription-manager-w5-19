package com.example.candidate;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/candidate")
public class CandidateController {

    @GetMapping
    public ResponseEntity<List<Candidate>> getCandidates() {
        List<Candidate> candidates = Arrays.asList(
                new Candidate("Eric", "DUPONT", "eric.dupont@gmail.com", "omnivorous", "male", "France", "SINGLE"),
                new Candidate("Julie", "DURANT", "julie.durant@gmail.com", "vegan", "female", "France", "DOUBLE")
        );
        return ResponseEntity.ok(candidates);
    }

}
