package com.example.candidate;
public class Candidate {
    private final String firstName;
    private final String lastName;
    private final String email;
    private final String diet;
    private final String gender;
    private final String country;
    private final String accommodation;

    public Candidate(String firstName, String lastName, String email, String diet, String gender, String country, String accommodation) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.diet = diet;
        this.gender = gender;
        this.country = country;
        this.accommodation = accommodation;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDiet() {
        return diet;
    }

    public String getCountry() {
        return country;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }

    public String getAccommodation() {
        return accommodation;
    }
}
